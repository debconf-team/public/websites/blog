DebConf11: Registrations and Call for Contributions
meta-author: Vedran Omeragic

<h2 style="text-align: center;"><span style="color: #003366;">Registrations for DebConf11 are now open!</span></h2>
<p style="text-align: justify;">Dear guests, attendees and contributors, we are happy to announce that registrations for this year's Conference are now open. The instructions for registration process can be found under <b><a href="http://debconf11.debconf.org/register.xhtml">Register page</a></b>. While the basic registrations will not close until the end of the Conference, sponsored registrations however, have the deadline. If you would like to apply for sponsored attendance, then it is imperative that you do it before <b>8th May</b>! Should you have any further questions regarding registrations, please mail them at <b><a href="mailto:registration@debconf.org">registration@debconf.org</a></b>.</p>

<h2 style="text-align: center;"><span style="color: #003366;">Call for Contributions!</span></h2>
<p style="text-align: justify;">We invite submissions of proposals for papers, presentations, discussion sessions and tutorials for Debconf11. Submissions don't have to be limited to traditional talks, you could propose a performance, art installation, debate, or anything else. Official submissions will be accepted until <b>May 8th 2011, 23:59 UTC</b>. For more information, please visit <b><a href="http://debconf11.debconf.org/cfp.xhtml">Call for contributions</a></b> page.</p>
