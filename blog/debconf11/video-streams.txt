DebianDay about to start / Video Streams are now available
meta-author: Alexander Reichle-Schmehl

Sorry for the late notification about the video streams.  We've been rather
busy down here.  However, DebianDay is about to start, and it looks, like
it will turn out to be a great event!

The Video Setup is finished, too.  The easiest way to watch the streams and
to participate via irc is by using the <q>Web 2.0</q> thingy at <a
href="http://debconf11.debconf.org/watch.xhtml">http://debconf11.debconf.org/watch.xhtml</a>.

You can also point your preferred video player at
<a
href="http://video.debconf.org:8000/Roundroom.ogv">http://video.debconf.org:8000/Roundroom.ogv</a>
for the smaller round room, or <a
href="http://video.debconf.org:8000/Auditorium.ogv">http://video.debconf.org:8000/Auditorium.ogv</a>
for the larger auditorium, where DebianDay will take place.

The full schedule for the entire conference is available at <a
href="http://penta.debconf.org/dc11_schedule/index.en.html">http://penta.debconf.org/dc11_schedule/index.en.html</a>.
As usual we'll make the videos available for download later.
