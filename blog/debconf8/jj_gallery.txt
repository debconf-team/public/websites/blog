DebConf Gallery
meta-author: Joerg Jaspert

As in previous years we have a <a
href="https://gallery.debconf.org/v/debconf8/">DebConf Gallery</a> running.


We would like to encourage people who have taken pictures around
DebCamp, DebConf and DebianDay to upload them to gallery.debconf.org.


The server is open for everyone but accounts need to be activated by
admins to avoid spam.  If you create an account and it isn't confirmed
within 2 hours, feel free to talk to one of the admins in person or on
IRC.
