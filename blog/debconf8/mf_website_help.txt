DebConf 8 website: a call for help
meta-author: Martín Ferrari

<p>DebConf 8 is approaching and our website is clearly not what we'd like it to
be. It turns out that compiling all the useful stuff that travellers might need
is not easy, so we kindly ask you &mdash;dear lazyweb&mdash;, to lend us a
hand.</p>

<p>The most important thing we need, is to recognise missing data. When you're
a local, is difficult to know what foreigners might need to know. So, even if
you don't know the country, your different point of view could help.</p>

<p>The second thing is to actually get articles written, some stuff ought to be
written by locals, but the majority can be researched from the web (as we
already did!). Proof-reading and checking translation is also appreciated.</p>

<p>Send us your patches, comments and suggestions to the <a
	href="mailto:website@rt.debconf.org">website queue</a> at the RT
system, or contact Tincho@OFTC. Don't sweat over formatting: plain text is OK,
if you want to provide markup, please use very spartan XHTML Strict.</p>
