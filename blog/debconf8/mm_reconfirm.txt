Reconfirmation deadline approaching
meta-author: Margarita Manterola

<p>June 15th is the last day to reconfirm your attendance to DebConf8. If you
are certain that you are coming, please reconfirm by setting the 'Reconfirm
Attendance' box in <a
href="https://penta.debconf.org/penta/submission/dc8/person">your PentaBarf's
account</a>.</p>

<p>If you have decided not to come to DebConf8, we are truly sorry about it,
but we ask you to please state this by unchecking the 'I want to attend this
conference box'.</p>

<p>Currently, 397 people have registered for DebConf8, 340 plan to attend, but
only 122 have reconfirmed (<a
href="http://munin.debconf.org/debconf.org/skinner.debconf.org-penta_attendee.html"
>live stats</a>).</p>
