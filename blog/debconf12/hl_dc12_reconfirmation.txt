Reconfirmation deadline tomorrow
meta-author: Holger Levsen

Just a quick reminder: if you haven't reconfirmed in our beloved pentabarf system, that you'll be attending DebConf12 in Managua in just a bit more then 2 weeks, then... do so now! The reconfirmation deadline is tomorrow!
