Call for DebConf14 venues!
meta-author: Gunnar Wolf

2000, Bordeaux, France. 2001, Bordeaux, France. 2002, Ottawa,
Canada. 2003, Oslo, Norway. 2004, Porto Alegre, Brazil. 2005,
Helsinki, Finland. 2006, Oaxtepec, Mexico. 2007, Edinburgh, United
Kingdom. 2008, Mar del Plata, Argentina. 2009, Cáceres, Spain. 2010,
New York, United States. 2011, Banja Luka, Bosnia and
Herzegovina. 2012, Managua, Nicaragua. 2013, Vaumarcus, Switzerland.

We have gone to all different latitudes (although not yet
longitudes!). We have had very hot and very cold weather. Great cities
and small villages have hosted us. What will be the next addition to
this great list?

As we approach DebConf12 (10 days to DebCamp and counting,
<em>yay!</em>), we should keep the future in mind. So, as every year
in recent history, we will once again have a DebConf session
presenting possible venues for the next year.

Organizing DebConf in your city means a <em>lot</em> of hard work. It
also means one of the greatest personal experiences you can
imagine. And it is a great way to contribute to Debian.

The decision process for every DebConf venue starts two years before,
with presentations during DebConf(n-2). That means, if you consider
presenting a bid for DebConf14, now is the moment to act!

Do you have to be present at Managua to propose your bid? No. You can
proxy via somebody — I'd suggest to do it via somebody who knows the
location you are suggesting, but basically, choose a friend that you
trust that trusts you. Of course, you can participate in the
presentation session via IRC.

Do you have to be a Debian Developer to propose a bid? No. For
DebConf9, none of the Cáceres guys was a DD; for DebConf10, some of
the people most involved from the local New Yorkers were not DDs. For
DC11, none of our hosts in Bosnia are DDs. And for DC12, the dear and
overworked Nicaraguan crew is also made from people interested in
getting closer to the Debian project, but not DDs.

Do you have to decide now? No. This is just a call for a first
presentation, but the decision regarding DC14 will be taken probably
around March 2013. However, giving a nice presentation at DebConf
helps a lot, gives you visibility, and will get the ball rolling.

Is there a geographical bias? Slight. So far, and since the second
DebConf, we have kept the tradition not to repeat continents on two
successive DebConfs. This is not a hard condition, however! While
there is some probability that DebConf14 will be somewhere in America
(the continent, of course), it depends on the proposals more than on
any pseudo-rule.

What do you need to start thinking about? Go visit <a
href="http://wiki.debconf.org/wiki/LocationCheckList">our prospective
location checklist</a>. You can also <a
href="http://wiki.debconf.org/wiki?title=Special%3ASearch&search=checklist&go=Go">look
at what other teams have historically presented</a>. And of course, go
to <a href="http://wiki.debconf.org/wiki/DebConf14">the DebConf14 wiki
planning page</a> — Register there, even if you are just in the early
phases of finding data.
