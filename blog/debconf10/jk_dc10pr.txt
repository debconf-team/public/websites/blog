DebConf10 dates and venue announced
meta-author: Jimmy Kaplowitz

The DebConf10 team just sent out a press release announcing the dates and venue
for <a href="http://debconf10.debconf.org">DebConf10</a> in <a
href="http://nycgo.com/">New York City</a>. Most of the readers of this blog
already saw it through some other list, so I'll just put the dates here and
provide the full text plus other relevant info via links.

<ul>
<li> Dates: July 25-31, 2010 will be DebCamp and August 1-7, 2010 will be DebConf.</li>
<li> <a href="http://lists.debconf.org/lurker/message/20091030.190443.b4c0f3dc.en.html">Press release text</a></li>
<li> <a href="http://www.eweek.com/c/a/Linux-and-Open-Source/DebConf10-Debian-Conference-Set-for-August-in-NYC-146412/">First press coverage in response to our announcement</a></li>
<li> <a href="http://debconf10.debconf.org">Main conference website</a></li>
<li> <a href="http://debconf10.debconf.org/visas.xhtml">Visa info</a></li>
<li> <a href="mailto:visa@debconf.org">Email address for visa help</a>(read the visa info page <em>before</em> emailing)</li>
<li> Yes, thanks to <a href="http://valessiobrito.info">Valessio Brito</a> we already have <a href="http://wiki.debconf.org/wiki/DebConf10/Artwork">"I'm going to DebConf10" buttons</a>.</li>
</ul>

We hope to see many of you there!
