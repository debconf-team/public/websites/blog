Well, at least it was memorable...
meta-author: Neil McGovern

I think people now appreciate the huge amount of effort that goes into creating such an event.

An indoor waterfall and a light show isn't the easiest thing in the world to create. Neither is an indoor paddling pool :)
