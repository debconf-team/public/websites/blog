TecKids @ DebConf: Register Until Tuesday
meta-author: DebConf Team

We are happy to announce that <a href="https://www.teckids.org/">TecKids</a>, a
non-profit specialising in working with kids, teaching them technonology, and
fostering self-sustaining communities amongst them, will be holding a workshop
from 15.08. to 18.08.

The focus is on kids aged 10-15 years old, but kids aged 8-16 years old are
welcome to attend if they can follow the course without supervision of theirs
parents.

Admission is free of charge, but registration through <a
href="https://www.teckids.org/froglabs_2015_debconf_anmeldung_en.htm">TecKids'
web form</a> is mandatory.  Registration is open until Tuesday, 11 August.

The rough schedule for now is:

<ul>
<li>robotics</li>
<li>game programming</li>
<li>Arduino</li>
</ul>

You will be kept up to date on any changes to the schedule and other details by
the TecKids team after registration. It is important that you check mail before
heading to DebConf in order to receive any last-minute informations.
