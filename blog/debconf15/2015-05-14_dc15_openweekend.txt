DebConf Open Weekend
meta-author: DebConf Content Team <content@debconf.org>

The first two days of this year's DebConf (August 15th and 16th) will
constitute the Open Weekend. On these days, we are planning to have the
Debian 22nd Birthday party, a Job Fair, and more than 20 hours of events
and presentations, including some special invited speakers.

Given that we expect to have a broader and larger audience during the
weekend, our goal is to have talks that are equally interesting for both
Debian contributors and users.

If you want to present something that might be interesting to the larger
Debian community, please go ahead and submit it. It can be for a talk of
either 45 or 20 minutes; if you don't have content for a full length talk,
we encourage you to go for the half length one. If you consider that the
event is better suited for either the OpenWeekend or the regular DebConf
days, you may say so in the comment field. But keep in mind that all
events might be rearranged by the content team to make sure they fit
together nicely.

<h3>Call for proposals</h3>

The deadline to submit proposals is June 15th. Please submit your talk
early with a good description and a catchy title. We look forward to
seeing your proposals!

If you want to submit an event please go ahead and read the original CfP on
DebConf15 <a href="http://debconf15.debconf.org/proposals.xhtml"
>http://debconf15.debconf.org/proposals.xhtml</a>.
