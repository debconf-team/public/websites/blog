DebConf13 final report
meta-author: Raphaël Walther, Didier Raboud, and the DebConf Team

The DebConf team is pleased to announce the release of the <a href="http://media.debconf.org/dc13/report/DebConf13-final-report.en.pdf">DebConf13 Final Report</a>. It's a 32-page document which gives the reader an idea about the conference as a whole.
It includes words from the Debian Project Leader, descriptions of talks, a section about the Debian Birthday Party, attendee impressions, budgeting and DebConf13 in numbers.
If you attended Debconf13, the report may refresh some of your memories and bring you closer to the organization team work.
If not, it will certainly encourage you to be part of future Debian events.

We thank the <a href="http://www.matanel.org/">Matanel foundation</a>, <a href="http://www.google.com/">Google</a>, the <a href="http://www.neuchateleconomie.ch/EN/Pages/default.aspx">Republic and Canton of Neuchâtel</a>, and <a href="http://debconf13.debconf.org/sponsors.xhtml">all other DebConf13 sponsors</a> for their support that made the event possible.

The DebConf team
