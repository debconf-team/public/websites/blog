DebConf13 matching fund
meta-author: Didier Raboud

As part of the <a href="http://debconf13.debconf.org/">DebConf13</a> fundraising effort, a <a href="http://www.brandorr.com">generous sponsor, Brandorr Group</a>, has proposed to start a <em>matching fund</em> in USD for DebConf13; in place through the end of April 30th. The rules are quite simple:

<ul>
<li>for each dollar donated by an individual to DebConf13 through the mechanism, <a href="http://www.brandorr.com/">Brandorr Group</a> will donate another dollar;</li>
<li>individual donations will be matched only up to <em>100 USD</em> each;</li>
<li>only donations in USD will be matched;</li>
<li><a href="http://www.brandorr.com/">Brandorr Group</a> will match the donated funds up to a maximum total of <em>5 000 USD</em>;</li>
<li>this generous offer will only stay in place through the <strong>end of April 30th</strong>, please act quickly, and help spread the word!</li>
</ul>

For details, please see our <a href="http://debconf13.debconf.org/monetary-support.xhtml">Monetary support from individuals</a> page, which also has a short url, for convenience: <a href="http://deb.li/dc13donate">http://deb.li/dc13donate</a>.

<h3>Help welcome</h3>

As always, the DebConf team is looking for volunteers. Some jobs need technical skills, but many DebConf tasks are about working to deadlines on non-technical issues (e.g. fundraising, budgeting, talk scheduling). You can see more information about some of the jobs to be done on the <a href="https://wiki.debconf.org/wiki/Jobs">DebConf wiki</a>.

Please do think about getting involved and sharing your ideas with us, to help us make DebConf an even more useful event for Debian in the future.

We look forward to welcoming you to Vaumarcus!

