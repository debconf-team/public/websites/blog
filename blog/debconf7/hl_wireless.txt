Wireless setup for DebConf7
meta-author: Holger Levsen

Only in the beginning of May it became clear that we need a new wireless 
sponsor this year, which quite quickly was found: <a href="http:www.freifunk.net">www.freifunk.net</a>, 
which is a german grassroot movement, connecting people and communities with mesh networks.


As some of them are also upstream OpenWRT developers, we didn't only get 19 
accesspoints, but I also got quite some intensive training, which resulted 
in this <a href="http://wiki.debconf.org/wiki/DebConf7/OpenWRT">wikipage with quite extensive documentation</a> and a nice 3D map, 
how+where+why to place accesspoints.


So we'll use the <a href="http://kamikaze.openwrt.org/">Kamikaze</a> version of OpenWRT, whereever possible. Despite 
it's name, Kamikaze is the new stable release of OpenWRT, the successor of 
WhiteRussian. 


The accesspoints will be simply running in bridge mode (and using DHCP to get 
their IP and not running a DHCP server themselves) and provide unencrypted 
wireless (authentification via chillispot, you'll have to login once via a
webfrontend, then your MAC address will be recorded and you don't need to 
login again) on the ESSID DebConf7. We'll have 802.11b+g everywere and 
additionally 802.11a in some places.


We calculated that for Teviot we'll need approximatly 15 accesspoints and three extra for 
the hackcenter we'll be using at night. As we got 19 from freifunk, we should have enough.
But I like to have some more, to be on the safe side and to be able to replace the least 
powerful ones with better ones.


If you can bring an accesspoint that runs OpenWRT, please send a short mail to 
debconf-infrastructure@lists.debconf.org. We prefer OpenWRT, so we can easily include them 
into our monitoring with munin and because it's a PITA to deal with lots of different
web interfaces for administration (and look up lots of different manuals on different
webpages...). If you really want to bring an AP which doesn't run OpenWRT, make sure 
the config described above is possible and please pre-configure it like that.


You can check <a href="http://wiki.openwrt.org/TableOfHardware">if your accesspoint can run OpenWRT</a>, a modell with broadcom or atheros chips would be best, simply 
because I already have preconfigured images. Alternativly, the above mentioned wikipage 
has instructions how to build an image. (And I have a build environment on my laptop. 
But that's easy to setup.)


On a personal note, I intend not to be much involved in the wireless setup this year
after unpacking the hardware, there is a nice admin team this year and I want to concentrate on
the video team and our work there.


And last but not least, but again: don't just plug your AP into the network in Teviot. God might kill a kitten, and 
we might loose our uplink if you do :-)
