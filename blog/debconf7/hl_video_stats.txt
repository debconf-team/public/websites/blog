Damn statistics ;-)
meta-author: Holger Levsen

Looking at the <a href="http://homer.dc7.debconf.org/munin/dc7.debconf.org/VideoSystem.dc7.debconf.org.html">munin stats of the video streams watched</a> (link only works from the DebConf7 network) today was a bit disappointing. About 40 viewers in average for all four rooms combined, worldwide and on the local network together. The upper talk room alone had a peek of 35 viewers today.


But I think (or hope?) the streams were and are still useful, since they allow Debian contributors to participate in DebConf, who otherwise couldn't. So if you're one of those, please <a href="mailto:debconf-video@lists.debconf.org">let us know</a> what you think about the streams, and recordings as well.


What I am really happy about is the software framework we created before and during DebCamp and DebConf. IMO it is really impressive and very very helpful, compared to previous workflows. Ben <a href="http://womble.decadent.org.uk/blog/dv-mixer-3">already</a> <a href="http://womble.decadent.org.uk/blog/debconf-video-software">blogged</a> a about it a bit. Unsurprisingly the <a href="http://wiki.debconf.org/wiki/DebConf7/videoteam/">documentation</a> can be improved still and it's not the only thing that can be improved. And we do have more great stuff in the pipeline for DebConf8 :-) 

